if test -n $HOME
  set NIX_LINK "$HOME/.nix-profile"

  # Set the default profile
  if test ! -L "$NIX_LINK"
    echo "creating $NIX_LINK" >&2
    set _NIX_DEF_LINK /nix/var/nix/profiles/default
    /nix/store/xmlp6pyxi6hi3vazw9821nlhhiap6z63-coreutils-8.24/bin/ln -s "$_NIX_DEF_LINK" "$NIX_LINK"
  end

  # Subscribe the user to the Nixpkgs channel by default.
  if test ! -e $HOME/.nix-channels
    echo "https://nixos.org/channels/nixpkgs-unstable nixpkgs" > $HOME/.nix-channels
  end

  # Append ~/.nix-defexpr/channels/nixpkgs to $NIX_PATH so that
  # <nixpkgs> paths work when the user has fetched the Nixpkgs
  # channel.
  set -x NIX_PATH $NIX_PATH nixpkgs=$HOME/.nix-defexpr/channels/nixpkgs

  # Set $SSL_CERT_FILE so that Nixpkgs applications like curl work.
  if test -e /etc/ssl/certs/ca-bundle.crt
    set -x SSL_CERT_FILE /etc/ssl/certs/ca-bundle.crt
  else if test -e /etc/ssl/certs/ca-certificates.crt
    set -x SSL_CERT_FILE /etc/ssl/certs/ca-certificates.crt
  else if test -e $NIX_LINK/etc/ca-bundle.crt
    set -x SSL_CERT_FILE $NIX_LINK/etc/ca-bundle.crt
  end
end

set -x PATH $HOME/.nix-profile/bin $PATH
set -x PATH /usr/local/go/bin $PATH
set -x GOPATH $HOME/prog/go
